//function to create users staying in Germany
function stayingInGermany(object) {
  //creating array of keys of object
  let usersKeys = Object.keys(object);

  //filtering the usersKeys who lives in Germany
  let usersArray = usersKeys.filter((user) => {
    return object[user]["nationality"] === "Germany";
  });

  return usersArray;
}

//exporting the above function
module.exports = stayingInGermany;
