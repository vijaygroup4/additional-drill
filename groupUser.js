// Function to group the users based on designation
function groupUser(object) {
  //extracting keys and creating array
  let usersKeys = Object.keys(object);

  //using reduce function to return object containing designation along with respective users
  let groupUserObject = usersKeys.reduce((newObject, user) => {
    const designation = object[user]["designation"];

    // Using if-else statements to handle different designations
    if (designation.includes("Golang")) {
      if (newObject.hasOwnProperty("Golang")) {
        newObject["Golang"].push(user);
      } else {
        newObject["Golang"] = [user];
      }
    } else if (designation.includes("Python")) {
      if (newObject.hasOwnProperty("Python")) {
        newObject["Python"].push(user);
      } else {
        newObject["Python"] = [user];
      }
    } else {
      if (newObject.hasOwnProperty("Javascript")) {
        newObject["Javascript"].push(user);
      } else {
        newObject["Javascript"] = [user];
      }
    }

    return newObject;
  }, {});

  //returning the groupUserObject
  return groupUserObject;
}

// Exporting the above function
module.exports = groupUser;
