//function to create users playing video games
function videoGames(object) {
  //creating the array of keys of object
  let usersKeys = Object.keys(object);

  //filtering the users who only play video games
  let usersArray = usersKeys.filter((user) => {
    return object[user]["interests"][0].includes("Video") === true;
  });

  return usersArray;
}

//exporting the above function
module.exports = videoGames;
