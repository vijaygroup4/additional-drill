//function to compare the values
function compareByLevelAndAge(user1, user2) {
  //Assigning numerical values,so it will become easy for difference calculation
  let levelValues = {
    Senior: 10,
    Python: 5,
    Intern: 0,
  };

  //calculating the difference value
  let differenceValue =
    levelValues[user2.designation.split(" ")[0]] -
    levelValues[user1.designation.split(" ")[0]];

  //if they dont have same designation,sorting happens based on their designation in descending order
  //the difference value is negative to sort in descending order
  if (differenceValue !== 0) {
    return differenceValue;
  }

  //if they have same designation,the sorting happens based on their age in descending order
  return user2["age"] - user1["age"];
}

//function to sort the users based on designation,age
function sort(object) {
  //creating array of keys of object
  let usersArray = Object.keys(object);

  //applying sort function to sort
  usersArray = usersArray.sort((userName1, userName2) => {
    return compareByLevelAndAge(object[userName1], object[userName2]);
  });

  return usersArray;
}

//exporting the above sort function
module.exports = sort;
