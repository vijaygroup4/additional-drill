//function to return users if they do Masters
function masters(object) {
  //creating array of keys of object
  let usersKeys = Object.keys(object);

  //filtering the users who only done masters
  let mastersArray = usersKeys.filter((user) => {
    return object[user]["qualification"] === "Masters";
  });

  return mastersArray;
}

//exporting the above function
module.exports = masters;
